from django.shortcuts import render
from .models import CandidateTags,Tag_master,Candidate,Tagged
from .serializers import TagSerializers,CandidateSerializer,TaggedSerializer,TagSerializers,TagSuggestions
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.generics import CreateAPIView,ListAPIView,ListCreateAPIView
from rest_framework import generics
# Create your views here.

class TagList(ListAPIView):
    queryset = CandidateTags.objects.all()
    serializer_class = TagSerializers
                
class TagCreate(CreateAPIView):
    queryset = CandidateTags.objects.all()
    serializer_class = TagSerializers

    def post(self,request,*args,**kwargs):
        # Convert  the string into list
        list_of_tags = request.data['tag'].split(",")
        for tag in list_of_tags:
            # check if the tag is present in the Tag_master (which is used for suggestions)
            Tag_master.objects.get_or_create(title=tag)
        # Return the tags in a list and store it in the candidate table
        CandidateTags.objects.create(tag=str(list_of_tags))
        return Response(data=request.data["tag"].split(","), status=200)

class Tagthem(generics.CreateAPIView):
    # Endpoint for through table.
    # http://127.0.0.1:8000/api/v1/tagthem/
    serializer_class = TaggedSerializer
    queryset = Candidate.objects.all()

class CandidateList(generics.ListCreateAPIView):
    # List all the candidates (GET) 
    # Endpoint :- http://127.0.0.1:8000/api/v1/candidates/
    # Create candidates (POST) 
    # Endpoint :- http://127.0.0.1:8000/api/v1/candidates/
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

    def get_serializer_class(self):
        switcher = {
            'GET' : CandidateSerializer,
            'POST' : CandidateSerializer,
        }
        return switcher.get(self.request.method)

class TagSuggestion(generics.ListAPIView):
    serializer_class = TagSuggestions
    queryset = Tag_master.objects.all()