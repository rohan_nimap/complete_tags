from django.urls import path
from . import views

urlpatterns = [
    path('tags/create/', views.TagCreate.as_view()),
    path('tags/', views.TagSuggestion.as_view()),
    path('candidates/', views.CandidateList.as_view()),
    path('tagthem/',views.Tagthem.as_view()),
]