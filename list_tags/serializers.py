from rest_framework import serializers
from . import models

class TagSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.CandidateTags
        fields = '__all__'

class TaggedSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Tagged

class CandidateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Candidate
        depth = 1 #

class TagSuggestions(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Tag_master