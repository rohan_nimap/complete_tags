from django.db import models
import json

class Tag_master(models.Model):
    title = models.CharField(max_length=200,unique=True)

    def __str__(self):
        return self.title

    class __Meta__:
        db_table = 'tags_master'

class CandidateTags(models.Model):
    tag = models.CharField(max_length=200) # the tag name, make unique=True to not allow duplicate values.
    candidate = models.ManyToManyField('Candidate',through="Tagged",related_name="tags_candidate") # candidate Foreign Key

    def __str__(self):
        return self.tag

    def set_tag(self, x):
        self.tag = json.dumps(x)

    def get_tag(self):
        return json.loads(self.tag)

    
    class __Meta__:
        db_table = 'list_tags'

class Candidate(models.Model):
    name = models.CharField(max_length=200) # Name of the candidate.
    tags = models.ManyToManyField('CandidateTags',through='Tagged',related_name="candidate_tags") # tags Foreign Key

    def __str__(self):
        return self.name

    class Meta:
        db_table = "candidate"

class Tagged(models.Model):
    # the through table which joins the foreign keys of other two tables.
    tags = models.ForeignKey(Candidate,on_delete=models.CASCADE,null=True)
    Candidates = models.ForeignKey(CandidateTags,on_delete=models.CASCADE,null=True)