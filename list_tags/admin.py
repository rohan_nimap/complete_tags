from django.contrib import admin
from .models import CandidateTags,Tag_master,Candidate,Tagged
# Register your models here.
admin.site.register(CandidateTags)
admin.site.register(Tag_master)
admin.site.register(Candidate)
admin.site.register(Tagged)