# Generated by Django 2.2.4 on 2019-12-23 07:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'candidate',
            },
        ),
        migrations.CreateModel(
            name='CandidateTags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Tag_master',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tagged',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Candidates', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='list_tags.CandidateTags')),
                ('tags', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='list_tags.Candidate')),
            ],
        ),
        migrations.AddField(
            model_name='candidatetags',
            name='candidate',
            field=models.ManyToManyField(related_name='tags_candidate', through='list_tags.Tagged', to='list_tags.Candidate'),
        ),
        migrations.AddField(
            model_name='candidate',
            name='tags',
            field=models.ManyToManyField(related_name='candidate_tags', through='list_tags.Tagged', to='list_tags.CandidateTags'),
        ),
    ]
