from django.apps import AppConfig


class ListTagsConfig(AppConfig):
    name = 'list_tags'
